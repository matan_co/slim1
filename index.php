<?php
require "vendor/autoload.php";
$app = new \Slim\App();
$app->get('/customers/{id}', function($request, $response,$args){
    $json = '{"1":"john", "2":"jack"}';
    $array = (json_decode($json, true));
    if(array_key_exists($args['id'], $array)){
        echo $array[$args['id']];
    }
    else{
        echo "User not found.";
    }

});
$app->run();
